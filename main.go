package main

import (
	"net/http"
	"encoding/json"
	"bytes"
	"github.com/go-redis/redis"
	"github.com/magiconair/properties"
	"github.com/buger/jsonparser"
	"io/ioutil"
	"strings"
)

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func getParameter()map[string]string{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	username:=p.GetString("twilio.sid","AC8e0e5aa72d449f96747198b3aaa4da44")
	password:=p.GetString("twilio.key","0e978a864b0a468c9f94111f6b4a07de")
	timeout:=p.GetString("twilio.timeout","5")
	from:=p.GetString("twilio.from","6285574679971")
	return map[string]string{
		"Username": username,
		"Password": password,
		"Timeout": timeout,
		"From": from,
	}
}

func main() {
	http.HandleFunc("/digiroin/service/pulsa", pulsa)
	http.ListenAndServe(":7050", nil)
}

type Pulsa struct {
	Cashtag string 	`json:"cashtag"`
	Phone string	`json:"phone"`
	Denom string	`json:"denom"`
}

func pulsa(w http.ResponseWriter, r *http.Request) {
	response := Pulsa{}
	client := &http.Client{}
	redis := initRedis()
	param := getParameter()
	err := json.NewDecoder(r.Body).Decode(&response)
	result :=""
	if(err!=nil){
		w.WriteHeader(http.StatusBadRequest)
		result = `{"Error":"`+err.Error()+`"}`
	}else{
		redisValue := redis.HGet("service","pulsa$"+response.Cashtag).Val()
		if(redisValue==""){
			result = `{"Error":"cashtag not found"}`
			w.WriteHeader(http.StatusBadRequest)
		}else{
			req, err := http.NewRequest("GET", "https://lookups.twilio.com/v1/PhoneNumbers/"+response.Phone+"?Type=carrier&Type=caller-name", nil)
			req.SetBasicAuth(param["Username"],param["Password"])
			req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
			resp, err := client.Do(req)
			if(err==nil){
				if(resp.StatusCode==200){
					rs ,_:=ioutil.ReadAll(resp.Body)
					country, _:= jsonparser.GetString(rs, "country_code")
					if(country!="ID"){
						w.WriteHeader(http.StatusNotFound)
						result = `{"Error":"invalid number"}`
						w.Write([]byte(result))
						return
					}else {
						operatorValue := redis.HGet("pulsa",response.Phone)
						if(operatorValue.Val()==""){
							networkCode, _ := jsonparser.GetString(rs, "carrier", "mobile_network_code")
							operatorValue = redis.HGet("operator",networkCode)
						}
						bodyReq := `{"Phone":"`+response.Phone+`","Type":"`+operatorValue.Val()+`","Denom":"`+response.Denom+`"}`
						var jsonStr = []byte(bodyReq)
						req, err := http.NewRequest("POST", redisValue, bytes.NewBuffer(jsonStr))
						resp, err := client.Do(req)
						if(err==nil){
							res,err := ioutil.ReadAll(resp.Body)
							if err==nil{
								if resp.StatusCode==200{
									redis.HSet("pulsa",response.Phone,operatorValue.Val())
									error,_ := jsonparser.ParseString(res)
									result = error
									go info("Info",`success buy pulsa prepaid for number `+response.Phone+` with denom `+response.Denom+`from `+response.Cashtag,"200")
								}else{
									error,_ := jsonparser.ParseString(res)
									result =error
									w.WriteHeader(http.StatusInternalServerError)
									go info("Error",error,"500")
								}
							}else{
								result = `{"Error":"`+err.Error()+`"}`
								w.WriteHeader(http.StatusInternalServerError)
								go info("Error",err.Error(),"500")
							}
						}else{
							result = `{"Error":"`+err.Error()+`"}`
							w.WriteHeader(http.StatusInternalServerError)
							go info("Error",err.Error(),"500")
						}
					}
				}else {
					w.WriteHeader(http.StatusBadRequest)
					result = `{"Error":"invalid phone number"}`
					go info("Error",`invalid phone number`,"400")
				}
			}else{
				w.WriteHeader(http.StatusInternalServerError)
				result = `{"Error":"`+err.Error()+`"}`
				go info("Error",err.Error(),"500")
			}
		}
	}
	w.Write([]byte(result))
}

func info(typeInfo string, message string, code string){
	body := strings.NewReader(`{ "type" :"`+typeInfo+`" ,"message" : "`+message+`","code" : "`+code+`" }`)
	req, err := http.NewRequest("POST", "http://logs-01.loggly.com/inputs/5d973c32-dcab-46d9-83f1-85f7a0000ed9/tag/pulsa-service/", body)
	if err != nil {
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
	}
	defer resp.Body.Close()
}